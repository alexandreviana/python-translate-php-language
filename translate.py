import os, requests, time, sys, json, string, re
def traduz(texto):
    api_key = "YOUR_GOOGLE_API_KEY"
    r = requests.get(
        "https://translation.googleapis.com/language/translate/v2",
        params = {
            "key": api_key,
            "q": texto,
            "target": "pt",
            "source":"en"
        }
    )
    resposta = json.loads(r.text)
    return(resposta['data']['translations'][0]['translatedText'])

arq_origem = open("site_lang.php", "r") 
arq_escrita = open("site_lang_pt.php", "a")
for line in arq_origem: 
    texto_1 = line.split(" = ")
    texto_fo = re.findall('"([^"]*)"', texto_1[1])
    texto_final = line.replace(texto_fo[0],traduz(texto_fo[0]))
    arq_escrita.write(texto_final)